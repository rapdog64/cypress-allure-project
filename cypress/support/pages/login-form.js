class LoginForm {

    openLoginForm() {
        cy.allure().startStep("Open login form");
        cy.visit("/login")
        cy.allure().endStep();
        return this;
    }

    typeEmail(email) {
        cy.allure().parameter("email", email).step("Type password");
        cy.get("#Email").type(email);
        return this;
    }

    typePassword(password) {
        cy.allure().parameter("password", password).step("Type password");
        cy.get("#Password").type(password);
        return this;
    }

    clickLoginButton() {
        cy.allure().step("Click the login button");
        cy.get(".login-button").click();
        return this;
    }

    getUserProfileName() {
        cy.allure().step("Get user profile name");
        return cy.get(".account")
    }
}

export default LoginForm