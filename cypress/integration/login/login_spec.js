import LoginForm from "../../support/pages/login-form";

describe("", () => {

    it("login test", () => {
        cy.allure().testID("5745783465")
            .severity("critical").feature("Create assortment").story("Assortment story")
            .suite("Assortment suite").description("Create assortment with invalid data")
            .link("google.com", "google").issue("jira","JIRA-1234").tag("reg");


        const loginForm = new LoginForm();
        loginForm.openLoginForm()
            .typeEmail("testing@email.ru")
            .typePassword("testing")
            .clickLoginButton()
            .getUserProfileName()
            .should("contain.text", "testing@email.ru");
    });


    it("random test", () => {
        cy.allure().testID("5745783465")
            .severity("critical").feature("Create assortment").story("Assortment story")
            .suite("Assortment suite").description("Create assortment with invalid data")
            .link("google.com", "google").issue("jira","JIRA-1234").tag("smoke");
    });

});